# Pincer

## Continuous Delivery

Minden merge request lezárásakor újabb folyamatos elérhetővé tétel (CD) történik. Ez statikusan a 
https://pincer-projekt.herokuapp.com/waiter/ oldalon elérhető minden esetben. Bővebb információkért olvassa el
a Hasznalati_utasitas.docx dokumentumban leírtakat!


## Python telepítés

Az anaconda.org oldalról töltsd le a számítógépednek és az operációsrendszerednek megfelelő anaconda telepítőt. A telepítő program végig vezet a telepítésen.

## Django telepítés

Miután feltelepítetted a Python-t az Anaconda-val, a parancssorba írd be a következőket:

conda install -c conda-forge django django-ninja

## Pincér telepítése

Klónozd le a waiter-t a gyökérkönyvtárba és utána tedd a következőket:

python manage.py migrate

python manage.py createsuperuser
--utána meg kell válaszolni kérdéseket, lehetséges válaszok:
    superuser: admin
    e-mail: admin@domain.com
    jelszó: tetszőleges, de ne túl bonyolult, mert jelenleg nincs jelentősége

python manage.py makemigrations waiter

python manage.py migrate

python manage.py runserver

Használható oldalak: 
http://localhost:8000/admin
http://localhost:8000/waiter

Ezekből további oldalakba lehet eljutni.
Ha bármi probléma van írjatok erre az e-mail címre: csafanni9@gmail.com

