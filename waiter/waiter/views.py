from django.shortcuts import render, HttpResponse, loader, HttpResponseRedirect
from .models import (Waiter, Kitchen, Product, Order,
                     Status, Table, Component, Boss)
from .forms import WaiterLogin


def index(request):
    """
    Kiírja a pincérek neveit és a hozzájuk tartozó
    linkeket kiírja a kezdő oldalra.
    Csak tesztelés és kísérleti célokkal van bent.
    """
    latest_waiter_list = Waiter.objects.order_by('name')[:5]
    template = loader.get_template('waiter/index.html')
    context = {
        'latest_waiter_list': latest_waiter_list,
    }
    return HttpResponse(template.render(context, request))


def get_users(request):
    """
    Megjelenít egy bejelentkezési formot és a bejelentkezési adatok alapján
    eldönti milyen további oldalakra kell tovább küldenie.
    """
    if request.method == 'POST':
        form = WaiterLogin(request.POST)
        if form.is_valid():
            latest_waiter_list = Waiter.objects.order_by('name')
            current_kitchen = Kitchen.objects.order_by('id')
            current_boss = Boss.objects.order_by('id')
            """
            Konyhába való bejelentkezés
            """
            if ("konyha" == form.data['name'] and
                    current_kitchen[0].password == form.data['password']):
                return HttpResponseRedirect('/waiter/kitchen/')
            if ("főnök" == form.data['name'] and
                    current_boss[0].password == form.data['password']):
                return HttpResponseRedirect('/waiter/boss/')
            """
            Pincérbe való bejelentkezés
            """
            new_id = -1
            correct_login = False
            for i in latest_waiter_list:
                if (
                    i.name == form.data['name'] and
                        i.password == form.data['password']):
                    correct_login = True
                    new_id = i.id
            if correct_login:
                link = '/waiter/' + str(new_id) + '/'
                return HttpResponseRedirect(link)
            """
            Helytelen bejelentkezés
            """
            return HttpResponseRedirect('/waiter/notvalid/')
    else:
        form = WaiterLogin()

    return render(request, 'polls/login.html', {'form': form})


def notvalid(request):
    """
    Helytelen bejelentkezés esetén legenerálja a hiba oldalt.
    """
    return render(request, 'polls/notvalid.html')


def kitchen(request):
    """
    Legenerálja a konyha oldalát.
    Kiírja leadott státuszú megrendelések
    neveit és a hozzájuk tartozó linkeket.
    """
    latest_orders_list = Order.objects.order_by('id')

    return render(
        request, 'polls/kitchen.html',
        {'latest_orders_list': latest_orders_list, 'status': Status.ordered})


def waiter(request, waiter_id):
    """
    Felépíti az adott pincér oldalát.
    A következő linkeket tartalmazza:
        -rendelések felvétele
        -leadott rendelések
        -elkészült rendelések
        -kivitt rendelések
    """
    latest_waiter_list = Waiter.objects.order_by('id')
    latest_order_list = Order.objects.order_by('id')

    for i in latest_waiter_list:
        if i.id == waiter_id:
            name = i.name

    return render(
        request, 'polls/waiter.html', {
            'name': name, 'waiter_id': waiter_id,
            'latest_order_list': latest_order_list})


def ordering(request, waiter_id):
    """
    Megjelenít egy formot, amiben a pincér megadhatja a rendelés asztalát,
    és a rendelt termékeket és azok darab számát. Még nem működképes.
    """
    latest_product_list = Product.objects.order_by('id')

    current_product_list = list(latest_product_list)

    latest_table_list = Table.objects.order_by('id')
    latest_order_list = Order.objects.order_by('id')
    latest_waiter_list = Waiter.objects.order_by('id')

    current_order = Order()
    current_order.status = Status.ordered

    order_list = list(latest_order_list)

    if not bool(order_list):
        current_order.number = 1
    else:
        my_ord = order_list[-1]
        current_order.number = str(int(my_ord.number)+1)

    current_waiter = [i for i in latest_waiter_list if i.id == waiter_id][0]
    current_order.waiter = current_waiter
    counter = -1
    tmp = request.POST
    for post_data in tmp:
        if post_data != 'csrfmiddlewaretoken':
            if post_data == 'radio':
                current_order.table = [
                    table for table in latest_table_list
                    if table.id == int(tmp['radio'])][0]
                current_order.save()
            else:
                counter = counter + 1
                if tmp[post_data] != '':
                    component = Component()
                    component.pieces = tmp[post_data]
                    component.product = current_product_list[counter]
                    component.order = current_order
                    component.save()

    return render(
        request, 'polls/ordering.html', {
            'latest_product_list': latest_product_list,
            'latest_table_list': latest_table_list, 'waiter_id': waiter_id})


def ordered_orders(request, waiter_id):
    """
    Kiírja adott pincér leadott státuszú megrendeléseinek
    neveit és a hozzájuk tartozó linkeket.
    """
    latest_orders_list = Order.objects.order_by('id')
    name = "Leadott"

    return render(
        request, 'polls/orders.html', {
            'latest_orders_list': latest_orders_list,
            'waiter_id': waiter_id, 'status': Status.ordered, 'name': name})


def done_orders(request, waiter_id):
    """
    Kiírja adott pincér elkészült státuszú megrendeléseinek
    neveit és a hozzájuk tartozó linkeket.
    """
    latest_orders_list = Order.objects.order_by('id')
    name = "Elkészült"

    return render(
        request, 'polls/orders.html', {
            'latest_orders_list': latest_orders_list,
            'waiter_id': waiter_id, 'status': Status.done, 'name': name})


def served_orders(request, waiter_id):
    """
    Kiírja adott pincér kivitt státuszú megrendeléseinek
    neveit és a hozzájuk tartozó linkeket.
    """
    latest_orders_list = Order.objects.order_by('id')
    name = "Kivitt"

    return render(
        request, 'polls/orders.html', {
            'latest_orders_list': latest_orders_list,
            'waiter_id': waiter_id, 'status': Status.served, 'name': name})


def kitchen_order(request, order_id):
    is_kitchen = True
    latest_component_list = Component.objects.order_by('id')
    latest_order_list = Order.objects.order_by('id')
    for i in latest_order_list:
        if i.id == order_id:
            current_order = i

    total = 0
    for i in latest_component_list:
        if i.order.id == current_order.id:
            total = total + i.product.price * i.pieces

    if request.method == 'POST':
        if current_order.status == Status.ordered:
            current_order.status = Status.done
            current_order.save()
        else:
            if current_order.status == Status.done:
                current_order.status = Status.served
                current_order.save()
            else:
                if current_order.status == Status.served:
                    current_order.status = Status.paid
                    current_order.save()
        return HttpResponseRedirect("/waiter/kitchen/")

    return render(
        request, 'polls/order.html', {
            'order': current_order, 'total': total,
            'latest_component_list': latest_component_list,
            'is_kitchen': is_kitchen})


def order(request, waiter_id, order_id):
    """
    Felépíti adott rendelés oldalát.
    Kiírja adott rendelés sorszámát, státuszát,
    a pincért aki felvette, az asztalt ami rendelte,
    a rendelt termékeket és azok darab számát és a rendelés összegét.
    """
    is_kitchen = False
    latest_component_list = Component.objects.order_by('id')
    latest_order_list = Order.objects.order_by('id')
    for i in latest_order_list:
        if i.id == order_id:
            current_order = i

    total = 0
    for i in latest_component_list:
        if i.order.id == order_id:
            total = total + i.product.price * i.pieces

    if request.method == 'POST':
        if current_order.status == Status.ordered:
            current_order.delete()
            url = "/waiter/" + str(waiter_id) + "/"
            return HttpResponseRedirect(url)
        if current_order.status == Status.done:
            current_order.status = Status.served
            current_order.save()
        else:
            if current_order.status == Status.served:
                current_order.status = Status.paid
                current_order.save()
                url = "/waiter/" + str(waiter_id) + "/"
                return HttpResponseRedirect(url)
        url = "/waiter/" + str(waiter_id) + "/" + str(order_id) + "/"
        return HttpResponseRedirect(url)

    return render(
        request, 'polls/order.html', {
            'waiter_id': waiter_id, 'order': current_order,
            'total': total, 'latest_component_list': latest_component_list,
            'is_kitchen': is_kitchen})


def boss(request):
    return render(request, 'polls/boss.html')


def new_waiter(request):
    tmp = request.POST

    name = ""
    password = ""

    for post_data in tmp:
        if post_data == 'name':
            name = tmp['name']
        if post_data == 'password':
            password = tmp['password']

    if name != "" and password != "":
        current_waiter = Waiter()
        current_waiter.name = name
        current_waiter.password = password
        current_waiter.save()

    return render(request, 'polls/new_waiter.html')


def new_table(request):
    latest_waiter_list = Waiter.objects.order_by('id')

    tmp = request.POST
    name = ""

    for post_data in tmp:
        if post_data == 'name':
            name = tmp['name']
        if post_data == 'radio':
            current_waiter = [
                waiter for waiter in latest_waiter_list
                if waiter.id == int(tmp['radio'])][0]

    if name != "":
        current_table = Table()
        current_table.name = name
        current_table.waiter = current_waiter
        current_table.save()

    return render(
        request, 'polls/new_table.html',
        {'latest_waiter_list': latest_waiter_list})


def new_product(request):
    tmp = request.POST

    name = ""
    price = 0

    for post_data in tmp:
        if post_data == 'name':
            name = tmp['name']
        if post_data == 'price':
            price = tmp['price']

    if name != "" and price != 0:
        product = Product()
        product.name = name
        product.price = price
        product.save()

    return render(request, 'polls/new_product.html')


def waiter_list(request):
    latest_waiter_list = Waiter.objects.order_by('id')

    return render(
        request, 'polls/waiter_list.html',
        {'latest_waiter_list': latest_waiter_list})


def table_list(request):
    latest_table_list = Table.objects.order_by('id')

    return render(
        request, 'polls/table_list.html',
        {'latest_table_list': latest_table_list})


def product_list(request):
    latest_product_list = Product.objects.order_by('id')

    return render(
        request, 'polls/product_list.html',
        {'latest_product_list': latest_product_list})


def waiter_update(request, waiter_id):
    latest_waiter_list = Waiter.objects.order_by('id')

    current_waiter = [
        waiter for waiter in latest_waiter_list
        if waiter.id == waiter_id][0]

    tmp = request.POST

    for post_data in tmp:
        if post_data == 'delete':
            current_waiter.delete()

            return HttpResponseRedirect('/waiter/waiter_list')
        if post_data == 'name':
            current_waiter.name = tmp['name']
        if post_data == 'password':
            current_waiter.password = tmp['password']
        current_waiter.save()

    return render(
        request, 'polls/waiter_update.html',
        {'waiter': current_waiter})


def table_update(request, table_id):
    latest_table_list = Table.objects.order_by('id')
    latest_waiter_list = Waiter.objects.order_by('id')

    table = [table for table in latest_table_list if table.id == table_id][0]

    tmp = request.POST

    for post_data in tmp:
        if post_data == 'delete':
            table.delete()

            return HttpResponseRedirect('/waiter/table_list')
        if post_data == 'name':
            table.name = tmp['name']
        if post_data == 'radio':
            current_waiter = [
                waiter for waiter in latest_waiter_list if waiter.id ==
                int(tmp['radio'])][0]
            table.waiter = current_waiter
        table.save()

    return render(
        request, 'polls/table_update.html',
        {'table': table, 'latest_waiter_list': latest_waiter_list})


def product_update(request, product_id):
    latest_product_list = Product.objects.order_by('id')

    product = [
        product for product in latest_product_list
        if product.id == product_id][0]

    tmp = request.POST

    for post_data in tmp:
        if post_data == 'delete':
            product.delete()

            return HttpResponseRedirect('/waiter/product_list')
        if post_data == 'name':
            product.name = tmp['name']
        if post_data == 'price':
            product.price = tmp['price']
        product.save()

    return render(request, 'polls/product_update.html', {'product': product})


def kitchen_password(request):
    tmp = request.POST
    kitchen_list = Kitchen.objects.order_by('id')
    current_kitchen = kitchen_list[0]
    for post_data in tmp:
        if post_data == 'password':
            current_kitchen.password = tmp['password']
            current_kitchen.save()

    return render(
        request, 'polls/kitchen_password.html', {'kitchen': current_kitchen})


def boss_password(request):
    tmp = request.POST
    boss_list = Boss.objects.order_by('id')
    current_boss = boss_list[0]
    for post_data in tmp:
        if post_data == 'password':
            current_boss.password = tmp['password']
            current_boss.save()

    return render(request, 'polls/boss_password.html', {'boss': current_boss})
