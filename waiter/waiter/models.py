from django.db import models


class Waiter(models.Model):
    name: str = models.CharField(verbose_name="Név", max_length=200)
    password = models.CharField(verbose_name="Jelszó", max_length=200)


class Kitchen(models.Model):
    password = models.CharField(verbose_name="Jelszó", max_length=200)


class Table(models.Model):
    name: str = models.CharField(verbose_name="Név", max_length=20)
    waiter = models.ForeignKey(
        Waiter, on_delete=models.CASCADE, related_name="tables")


class Status(models.IntegerChoices):
    ORDERED = 1, 'leadott'
    DONE = 2, 'elkészült'
    SERVED = 3, 'kivitt'
    PAID = 4, 'kifizetett'


class Order(models.Model):
    status = models.PositiveSmallIntegerField(
        choices=Status.choices,
        default=Status.ORDERED
    )
    number: str = models.CharField(verbose_name="Szám", max_length=20)
    table = models.ForeignKey(
        Table, on_delete=models.CASCADE, verbose_name="Asztalok")
    waiter = models.ForeignKey(
        Waiter, on_delete=models.CASCADE,
        blank=True, null=True, related_name="orders")


class Product(models.Model):
    name: str = models.CharField(verbose_name="Név", max_length=200)
    price = models.PositiveIntegerField(verbose_name="Ár")


class Component(models.Model):
    product = models.ForeignKey(
        Product, on_delete=models.CASCADE, related_name="components")
    pieces = models.PositiveIntegerField(verbose_name="Darab szám")
    order = models.ForeignKey(
        Order, on_delete=models.CASCADE,
        related_name="products")


class Boss(models.Model):
    password = models.CharField(verbose_name="Jelszó", max_length=200)
