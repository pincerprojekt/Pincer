from django.test import TestCase

from .models import Waiter, Kitchen, Boss, Product, Table, Order, Component
from .forms import WaiterLogin


def test_set_up_waiter():
    Waiter(name="Lali", password="sajt")


def test_set_up_kitchen():
    Kitchen(password="sajt")


def test_set_up_boss():
    Boss(password="sajt")


def test_set_up_product():
    Product(name="sajt", price=300)


def test_set_up_table():
    waiter1 = Waiter(name="Lali", password="sajt")
    Table(name="asztal", waiter=waiter1)


def test_set_up_order():
    waiter1 = Waiter(name="Lali", password="sajt")
    table1 = Table(name="asztal", waiter=waiter1)
    Order(status=1, number="1", waiter=waiter1, table=table1)


def test_set_up_component():
    waiter1 = Waiter(name="Lali", password="sajt")
    table1 = Table(name="asztal", waiter=waiter1)
    order1 = Order(
        status=1, number="1", waiter=waiter1, table=table1)
    product1 = Product(name="sajt", price=300)
    Component(pieces=1, product=product1, order=order1)


class WaiterTestCase(TestCase):
    test_set_up_waiter()

    def test_waiter(self):
        waiter1 = Waiter(name="Lali", password="sajt")
        self.assertEqual(waiter1.password, "sajt")
        self.assertEqual(waiter1.name, "Lali")


class KitchenTestCase(TestCase):
    test_set_up_kitchen()

    def test_kitchen(self):
        kitchen1 = Kitchen(password="sajt")
        self.assertEqual(kitchen1.password, "sajt")


class BossTestCase(TestCase):
    test_set_up_boss()

    def test_boss(self):
        boss1 = Boss(password="sajt")
        self.assertEqual(boss1.password, "sajt")


class ProductTestCase(TestCase):
    test_set_up_product()

    def test_product(self):
        product1 = Product(name="sajt", price=300)
        self.assertEqual(product1.name, "sajt")
        self.assertEqual(product1.price, 300)


class TableTestCase(TestCase):
    test_set_up_table()

    def test_table(self):
        waiter1 = Waiter(name="Lali", password="sajt")
        table1 = Table(name="asztal", waiter=waiter1)
        self.assertEqual(table1.name, "asztal")
        self.assertEqual(table1.waiter.name, "Lali")
        self.assertEqual(table1.waiter.password, "sajt")


class OrderTestCase(TestCase):
    test_set_up_order()

    def test_order(self):
        waiter1 = Waiter(name="Lali", password="sajt")
        table1 = Table(name="asztal", waiter=waiter1)
        order1 = Order(status=1, number="1", waiter=waiter1, table=table1)
        self.assertEqual(order1.status, 1)
        self.assertEqual(order1.table.name, "asztal")
        self.assertEqual(order1.table.waiter.name, order1.waiter.name)
        self.assertEqual(order1.table.waiter.password, order1.waiter.password)


class ComponentTestCase(TestCase):
    test_set_up_component()

    def test_component(self):
        waiter1 = Waiter(name="Lali", password="sajt")
        table1 = Table(name="asztal", waiter=waiter1)
        order1 = Order(
            status=1, number="1", waiter=waiter1, table=table1)
        product1 = Product(name="sajt", price=300)
        component1 = Component(pieces=1, product=product1, order=order1)
        self.assertEqual(component1.pieces, 1)
        self.assertEqual(component1.product.name, "sajt")
        self.assertEqual(component1.product.price, 300)
        self.assertEqual(component1.order.status, 1)
        self.assertEqual(component1.order.table.name, "asztal")
        self.assertEqual(
            component1.order.table.waiter.name,
            component1.order.waiter.name)
        self.assertEqual(
            component1.order.table.waiter.password,
            component1.order.waiter.password)


class FormTestCase(TestCase):
    def test_form_is_valid(self):
        waiter1 = Waiter.objects.create(name="Lali", password="sajt")
        form = WaiterLogin({'name': "Lali", 'password': "sajt"})
        self.assertTrue(form.is_valid())
        self.assertEqual(form.data['name'], waiter1.name)
        self.assertEqual(form.data['password'], waiter1.password)

    def test_blank_form(self):
        form = WaiterLogin({})
        self.assertFalse(form.is_valid())
