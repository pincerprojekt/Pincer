from django.urls import path

from . import views

urlpatterns = [
    path('', views.get_users, name='index'),
    path('login/', views.get_users, name='login'),
    path('<int:waiter_id>/', views.waiter, name='waiter'),
    path('<int:waiter_id>/<int:order_id>/', views.order, name='order'),
    path('<int:waiter_id>/ordering', views.ordering, name='ordering'),
    path(
        '<int:waiter_id>/ordered_orders',
        views.ordered_orders,
        name='ordered_orders'),
    path('<int:waiter_id>/done_orders', views.done_orders, name='done_orders'),
    path(
        '<int:waiter_id>/served_orders',
        views.served_orders,
        name='served_orders'),
    path('kitchen/', views.kitchen, name='kitchen'),
    path('kitchen/<int:order_id>/', views.kitchen_order, name='kitchen_order'),
    path('notvalid/', views.notvalid, name='notvalid'),
    path('boss/', views.boss, name='boss'),
    path('new_waiter/', views.new_waiter, name='new_waiter'),
    path('new_table/', views.new_table, name='new_table'),
    path('new_product/', views.new_product, name='new_product'),
    path('waiter_list/', views.waiter_list, name='waiter_list'),
    path('table_list/', views.table_list, name='table_list'),
    path('product_list/', views.product_list, name='product_list'),
    path('waiter/<int:waiter_id>', views.waiter_update, name='waiter_update'),
    path('table/<int:table_id>', views.table_update, name='table_update'),
    path(
        'product/<int:product_id>',
        views.product_update,
        name='product_update'),
    path('kitchen_password/', views.kitchen_password, name='kitchen_password'),
    path('boss_password/', views.boss_password, name='boss_password'),
]
