from django.contrib import admin

from .models import Waiter, Table, Order, Kitchen, Product, Component, Boss

admin.site.register(Waiter)
admin.site.register(Table)
admin.site.register(Order)
admin.site.register(Kitchen)
admin.site.register(Product)
admin.site.register(Component)

admin.site.register(Boss)
