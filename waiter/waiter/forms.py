from django import forms


class WaiterLogin(forms.Form):
    name: str = forms.CharField(label='Felhasználó név', max_length=100)
    password: str = forms.CharField(
        label='Jelszó', max_length=10, widget=forms.PasswordInput())
